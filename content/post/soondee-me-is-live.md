+++
date = "2015-08-04T15:43:53+08:00"
title = "soondee.me is live"
+++

Finally got around to spin-up an EC2 instance to host my site. I always had an AWS account but never got into spinning up an instance or setting up a webserver to host my blog.
Until recently that I want to try Ansible's dynamic inventory that I decided to give it a go again. This site is the result of my attempt.

In this post I will share the tools and steps I took.

## The manual bits
The manual bit is of course,

- Sign-up a AWS account.
- (The bit I hoped I can automate in future) Create an EC2 instance, security group, keypairs.
(I'm not going to explain in details the steps, Amazon already had a good tutorial on how to do that)
- Tag your instance with the following key `blog_instance` with the value `1` which will be used by site.yml as host `tag_blog_instance_1`.

## The automatic bits
Once your EC2 instance is running,

- Clone the project from my [bitbucket](https://bitbucket.org/chuasoondee/aws-config-mgmt) repo. (Instructions are given in the `README.md` file)
- Edit the `site.yml` file to use your own custom tasks or roles e.g.
```yaml
    - hosts: tag_blog_instance_1
      tasks:
      - name: custom task
        command: uname -a
```
- Run `ansible-playbook -vvvv site.yml`

## Note on aws-config-mgmt repo
The project uses Ansible dynamic inventory to access your EC2 instances info and uses ansible roles `soondee.me` to deploy my static site.
If you plan to use the repo, do change the role accordingly.

## Resources
1. [AWS config management tools](https://bitbucket.org/chuasoondee/aws-config-mgmt)
2. [Ansible galaxy](https://galaxy.ansible.com/)
3. [EC2 getting started](https://aws.amazon.com/ec2/getting-started/)
